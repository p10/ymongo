var yaml = require('js-yaml');

function create(current) {
	return {
		hosts: current.hosts.split(','),
		username: current.username,
		password: current.password,
		database: current.database
	};
}

module.exports = function mongoise(yamlString, callback) {
	var doc = yaml.safeLoad(yamlString);

	if (!doc.mongo) {
		return callback(new Error('no mongo property'));
	}

	var mongos = [];

	if (doc.mongo.password) {
		mongos.push(create(doc.mongo));
	} else {
		Object.keys(doc.mongo).forEach(function(mongoName) {
			var mongo = create(doc.mongo[mongoName]);
			mongo.name = mongoName;
			mongos.push(mongo);
		});
	}

	callback(null, mongos);

};